function lg = simu_init_lengths(ring,famlist)
%SIMU_INIT_LENGTHS(RING,FAMILIES)
%
%Extract the length of the 1st magnet with the given family names into
%a strucure and return it
%
%RING:      AT structure
%FAMILIES:  Cell array of family names

lengths=cellfun(@family_length,famlist);    % Compute lengths
v=[famlist;num2cell(lengths)];              % Pack into a structure array
lg=struct(v{:});

    function ll=family_length(famname)
        lls=atgetfieldvalues(ring(atgetcells(ring,'FamName',['^' famname])),'Length');
        if isempty(lls)
            ll=NaN;
        else
            ll=lls(1);
        end
    end

end
