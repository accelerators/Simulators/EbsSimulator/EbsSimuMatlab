function [ring,all_fams]=apply_new_strength(ring,cellmag,val,errs)
% APPLY_NEW_STRENGTH(RING,CELLMAG,VAL,ERRS)
%
% According to what has been received from the Tango device, set the required
% magnet strengths
%
% The device send A DevVarDoubleStringArray with
% - strings being the magnet family name
% - doubles being first the number of magnets within each family followed by two
%   values for each magnet which are the magnet index in family (not in ring) and the
%   new magnet strength
%
%RING:		AT structure
%CELLMAG:	Structure with ring indices for each magnets in each magnet families
%VAL:		The new strength value(s)
%ERRS:      Table of errors ERRS.DK_K(:,1) = dipole field errors,
%                           ERRS.DK_K(:,2) = quadrupole field errors,
%
persistent vmode 
% msgdev=tango.Device('tango://ebs-simu:10000/sys/Ebs_simu_msg/01');
% msgdev=tango.Device('tango://ebs-simu:10000/sys/ringsimulator/ebs');

if isempty(vmode)
    vmode={... Family pattern, K field, index in K, sign
        '^Q[FDIJ][1-8][A-E]$','PolynomB',{2},1;...  1
        '^S[FDJI][12][A-E]$','PolynomB',{3},1;...  2
        '^O[FD][12][BD]$','PolynomB',{4},1;...  3
        '\w*HST','PolynomB',{1},-1;...4 
        '\w*VST','PolynomA',{1},1;... 5
        '\w*SKEW','PolynomA',{2},1;...6
        '^DQ[12].D','PolynomB',{1},-1;...7
        '^DQ[12].Q','PolynomB',{2},1;... 8
       };
end
nfam = size(vmode,1);
nb_fam=length(val.svalue);

if (nb_fam > 0)
    iend=nb_fam+2*cumsum(reshape(val.dvalue(1:nb_fam),nb_fam,1));
    ibeg=[nb_fam;iend(1:end-1)]+1;
    [cells,values]=arrayfun(@extract,ibeg,iend,'UniformOutput',false);
    all_fams=cellfun(@applyfam,val.svalue',cells,values,'UniformOutput',false);     % Apply to each family
else
    all_fams=struct;
end


    function [cell,value]=extract(i1,i2)
        v=reshape(val.dvalue(i1:i2),2,[]);
        cell=v(1,:);
        value=v(2,:);
    end

    function fam=applyfam(famname,cells,values)
        
        % chose family patterns
        mode=false(1,nfam);
        for imag=1:nfam
            mode(imag) = ~isempty(regexpi(famname,vmode{imag,1},'ONCE')); % logical mask
        end
        % get indexes
        minus=strfind(famname,'-');
        if ~isempty(minus) % correctors
            idlist=cellmag.(lower(famname(1:minus-1)))(cells);
        elseif famname(1)=='D' % DQ
            idlist=cellmag.(lower(famname(1:end-1)))(cells); % remove d/q at magnet end
        else  %all other magnets
            idlist=cellmag.(lower(famname))(cells);
        end
        
        magclass = vmode(mode,2:end);
        [fld,sindex,coef]=deal(magclass{:});
        
        % select only indexes of changed strengths (particularly usefull
        % if */*/all devices are manipulated
        indexes = [];
        
        for i = 1:length(idlist)
            dkk=errs.DK_K(idlist{i},sindex{1});
            if atgetfieldvalues(ring(idlist{i}),fld,sindex)~=coef*values(i)*(1+dkk)
                indexes = [indexes i];
                %msg = strcat(famname,' ',num2str(atgetfieldvalues(ring(idlist{i}),fld,sindex)),...
                %       ' ',num2str(coef*values(i)*(1+dkk)));
                %msgdev.msg = msg;
                %pause(1)
            end
        end
       
        idlist = idlist(indexes);
        values = values(indexes);
        
        if ~isempty(idlist)
          [fam_strs,nbs]=cellfun(@apply2,idlist,num2cell(values)','UniformOutput',false);
          fam=struct('name',{famname},'fam_ind',{cells},'at_strs',{fam_strs},'nbs',{nbs});
        else
          fam=struct;
        end
        
        function [simu_strs,nbs]=apply2(ids,value)
            % apply values from control system + errors
           
            % gradient errors
            dkk=errs.DK_K(ids,sindex{1});
            
            % set gradients in AT lattice
            ring(ids) = atsetfieldvalues(ring(ids),fld,sindex,coef*value*(1+dkk));           
            simu_strs=arrayfun(@(id) at2str(ring{id}),ids,'UniformOutput',false);
            nbs=length(ids);
        end
    end

end
