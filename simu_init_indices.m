function res=simu_init_indices(ring,id,famlist)
%SIMU_INIT_INDICES(RING,ID,FAMLIST)
%
%Retrieve indices in AT ring of all elements belonging to a set of families
%   
%RING:      	AT structure
%ID:			Indices of cell boundaries
%FAMLIST: 		List of families for which we want indices in ring

w=[famlist;famlist];    % Create a dictionary of search patterns
pattern=struct(w{:});
pattern.qf1a='^qf1[aj]'; % Modify to include injection cells
pattern.qd2a='^qd2[aj]';
pattern.qf2a='^qf2[aj]';
pattern.qd3a='^qd3[aj]';
pattern.qf4a='^qf4[aj]';
pattern.qf4e='^qf4[ei]';
pattern.qd3e='^qd3[ei]';
pattern.qf2e='^qf2[ei]';
pattern.qd2e='^qd2[ei]';
pattern.qf1e='^qf1[ei]';
pattern.sd1a='^s[dij]1a';
pattern.sf2a='^s[fij]2a';
pattern.sd1b='^s[dij]1b';
pattern.sd1d='^s[dij]1d';
pattern.sf2e='^s[fij]2e';
pattern.sd1e='^s[dij]1e';
pattern.of1b='^o[fij]1b';
pattern.of1d='^o[fij]1d';

%iend=find(id);              % locate cell boundaries in the structure array
%ibeg=[1;iend(1:end-1)+1];
ibeg=find(id);               % locate cell boundaries in the structure array (assume ID marker at cell start)
iend=[ibeg(2:end)-1;length(ring)];
idn=split(1:length(ring));  % split the element index cell by cell
indices=cellfun(@famindex,struct2cell(pattern)','UniformOutput',false);
v=[famlist;indices];        % Pack into a structure array
res=struct(v{:});

    function res=famindex(pattern)
        % return the indices of the selected magnets in each cell
        mask=split(atgetcells(ring,'FamName',['(' pattern '([-_]\d+)?)']));
        res={cellfun(@(i,j) i(j),idn,mask,'UniformOutput',false)};
    end

    function asplit=split(a)
        % split an array cell by cell
        asplit=arrayfun(@(i,j) a(i:j),ibeg,iend,'UniformOutput',false);
    end
end
