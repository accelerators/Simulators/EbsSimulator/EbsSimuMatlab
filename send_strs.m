function send_strs(allfams,dev)
% This function send magnet AT strings to the Tango
% simulator device. This is done in a loop for each Matlab
% family found in the allfams cell array

cellfun(@(x) foo(x,dev),allfams);

   function foo(fam_data,dev)
      sent.svalue=[fam_data.name fam_data.at_strs{:}];
      sent.lvalue=[length(fam_data.fam_ind) fam_data.fam_ind fam_data.nbs{:}];
      sent.lvalue=cast(sent.lvalue,'int32');
      dev.PushSimulatorStrings(sent); 
   end
end
