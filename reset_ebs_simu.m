function reset_ebs_simu( TangoHost, ringpath )
%RESET_EBS_SIMU( TangoHost,ringpath ) 
% sets back initial conditions for lattice with errors in the EBS simulator
% ! 
% from the directory indicated in the RingFile property of:
% tango://ebs-simu:10000/sys/ringsimulator/ebs
%
% load correction strengths stored in cortab table variable
% 
%see also:


if nargin < 1
TangoHost = ['tango://' getenv('TANGO_HOST')];

end

simdev=tango.Device([TangoHost '/sys/ringsimulator/ebs']);

%simdev.ErrMessage= ' ';

if nargin < 2
    ringfile=simdev.get_property('RingFile');
    ringpath=ringfile{1};
end
ringvarname=simdev.get_property('MatlabRingName');
varname=ringvarname{1};

if ischar(ringpath)
    [~,ringname]=fileparts(ringpath);
  
    r=load(ringpath);
    try
        ring=r.(varname); % use specified vvariable name
    catch
        ring=r.(ringname); %if not existing, use lattice file name
    end
    cortab = r.cortab; % correctors
else
    ring=ringpath;
end


try
    %reset bpm offsets and ref orbit
    bpmdev=tango.Device([TangoHost '/srdiag/bpm/all']);
    bpmdev.HOffsets = zeros(1,length(bpmdev.HOffsets.read));
    bpmdev.VOffsets = zeros(1,length(bpmdev.VOffsets.read));
    svd_h = tango.Device([TangoHost '/sr/beam-orbitcor/svd-h']);
    svd_v = tango.Device([TangoHost '/sr/beam-orbitcor/svd-v']);
    svd_h.BumpOrbit=zeros(1,length(svd_h.BumpOrbit.read));
    svd_v.BumpOrbit=zeros(1,length(svd_v.BumpOrbit.read));
    quads = tango.Device([TangoHost '/srmag/m-q/all']);
    sexts = tango.Device([TangoHost '/srmag/m-s/all']);
    skews = tango.Device([TangoHost '/srmag/sqp/all']);
    quads.ResonanceStrengths=zeros(1,length(quads.ResonanceStrengths.read));
    sexts.ResonanceStrengths=zeros(1,length(sexts.ResonanceStrengths.read));
    skews.ResonanceStrengths=zeros(1,length(skews.ResonanceStrengths.read));
catch errmsg
    disp(errmsg)
    warning('not possible to reset RefOrbit and BPM offsets')
end

%  Set Design Strengths to simulator
try
    pause('on')
    %DesignStrenghtsFile = '/segfs/tango/tmp/manu/DesignStrengths.csv';
    DesignStrenghtsFile = '/operation/beamdyn/matlab/optics/sr/theory/DesignStrengths.csv';

    qall = tango.Device([TangoHost '/srmag/m-q/all']);
    sall = tango.Device([TangoHost '/srmag/m-s/all']);
    oall = tango.Device([TangoHost '/srmag/m-o/all']);
    
    qall.Timeout = 150000;
    sall.Timeout = 150000;
    oall.Timeout = 150000;
    
    qall.DesignStrengthFile=DesignStrenghtsFile;
    sall.DesignStrengthFile=DesignStrenghtsFile;
    oall.DesignStrengthFile=DesignStrenghtsFile;

    pause(5)

catch errmes
    disp(errmes)
    warning('not possible to set DesignStrengthsFile Attribute')
end

% merge 2PW and transform back to single quadrupole
indQF8dip = find(atgetcells(ring,'FamName','QF8\w*') & atgetcells(ring,'Class','Bend'))';
ring=atsetfieldvalues(ring,indQF8dip,'BendingAngle',0);
ring=atsetfieldvalues(ring,indQF8dip,'Class','Quadrupole');
ring=atsetfieldvalues(ring,indQF8dip,'PassMethod','StrMPoleSymplectic4Pass');

precious=atgetcells(ring,'FamName','ID.*$|^JL1[AE].*') |...
        atgetcells(ring,'FamName','BM\w*') |...
        atgetcells(ring,'Class','Monitor');
[ring, kept] = atreduce(ring,precious);
cortab=cortab(kept,:);

%   Set quadrupole corrections
quadind=atgetcells(ring,'FamName','Q\w*','DQ\w*');%
setall('m-q/all',cortab.KL1n(quadind)');

%   Set sextupole corrections
sextind=atgetcells(ring,'FamName','S[FDIJ]\w*');
setall('m-s/all',cortab.KL2n(sextind)');

%   Set octupole corrections
octind=atgetcells(ring,'FamName','O[FIJ]\w*');
setall('m-o/all',cortab.KL3n(octind)');

maxsteer = 0.399*1e-3; % can go higher, PS from 2 to 2.4A maximum

%  Set h steerers 
correctorsH=atgetcells(ring,'FamName','^SH..|^S[DFIJ][12].','DQ\w*');%
%   Set horizontal steerers
corh = cortab.KL0n(correctorsH);
disp(['found ' num2str(length(find(abs(corh)>maxsteer))) ' H steerers above ' num2str(maxsteer*1e3) ' mrad'])
corh(corh>maxsteer) = maxsteer;
corh(corh<-maxsteer) = -maxsteer;
setcor('hst/all',corh,-1);

%  Set steerers and skew correctors
correctors=atgetcells(ring,'FamName','^SH..|^S[DFIJ][12].');
%   Set vertical steerers
corv = cortab.KL0s(correctors);
disp(['found ' num2str(length(find(abs(corv)>maxsteer))) ' V steerers above ' num2str(maxsteer*1e3) ' mrad'])
corv(corv>maxsteer) = maxsteer;
corv(corv<-maxsteer) = -maxsteer;
setcor('vst/all',corv,1);

%   Set skew quad correctors
setcor('sqp/all',cortab.KL1s(correctors),1);

%   Set the master source
rfcav=ring(atgetcells(ring,'Frequency'));
setms(rfcav(1));

%pause(5)

try
%   simdev.Init();
catch err
   disp('Time out error')
   disp(err.message)
end

    function setall(devnm,strengths)
        try
            ps=tango.Device([TangoHost '/srmag/' devnm]);
            ps.CorrectionStrengths=strengths;
            pause(4);
        catch err
%            names = ps.MagnetNames.read;
%            try
%                for in = 1:length(names)
%                    devname = char(names(in));
%                    dev = tango.Device([TangoHost '/' devname(end-16:end)]);
%                    dev.CorrectionStrength = strengths(in);
%                end
                
%            catch erragain
%                fprintf('%s\n',[err.message erragain.message]);
%            end
                
            fprintf('%s\n',err.message);
        end
    end

    function setcor(devnm,strengths,si)
        fprintf('%s (%d): %g\n',devnm, length(strengths),std(strengths));
        try
            ps=tango.Device([ TangoHost '/srmag/' devnm]);
            ps.Strengths=(strengths' * si);
            pause(6);
        catch err
            fprintf('%s\n',err.message);
        %    save(fileparts(devnm),'strengths');
        end
    end

    function setms(cav)
        freq = atgetfieldvalues(cav,'Frequency');
        fprintf('rf: %8f\n',freq);
        try
            ms = tango.Device([ TangoHost '/sy/ms/1']);
            ms.Frequency = freq;
        catch err
            fprintf('%s\n',err.message);
        end
    end

end



