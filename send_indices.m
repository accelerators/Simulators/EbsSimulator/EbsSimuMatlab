function send_indices(cellmag,dev)
%SEND_INDICES(CELLMAG,DEV)
%
%This function send magnet indices (in ring) to the Tango
%simulator device. This is done in a loop for each Matlab
%family found in the cellmag structure
%The matlab family names are the cellmag structure field names
%
%CELLMAG:	Structure with ring indices for each magnets in each magnet families	
%DEV:		Tango device

fams=fieldnames(cellmag);
dat=struct2cell(cellmag);
cellfun(@(x,y) foo(x,y,dev),fams,dat);

   function foo(fam_name,inds,dev)
   sent.svalue={fam_name};
   lengthes=cellfun(@length,inds);
   sent.lvalue=cat(2,lengthes',inds{:});
   sent.lvalue=cast(sent.lvalue,'int32');
   dev.PushSimulatorIndexes(sent); 
   end
end
