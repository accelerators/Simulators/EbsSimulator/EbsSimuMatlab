function ebs_simu(ringpath)
%EBS_SIMU(RINGPATH)
%
% This function is the root Matlab function for the ESRF-EBS ring
% In pseudo-code, this function is doing
%
% - Init miscellaneous variables
% - Send some init values to Tango world
% - /WHILE/ true
%	- Get new value(s) from Tango world
%	- Apply them on simulated equipments
%	- Compute ring parameters
%	- Send them to Tango world
% - /END WHILE/
%
% Function input arg(s)
% RINGPATH: 	Full path of ring definition file. If this arg is not given, retrieve it from the Tango world
%(it's a Tango device property)
%
% This function does not return anything
%

f1={'qf1a','qd2a','qd3a','qf4a','qd5b','qf6b','qf8b',...
    'sd1a','sf2a','sd1b','of1b','sh1a','sh2b','sh3e','dq1b','dq2c'};
f3={'qf1j','qd2j','qf2j','qd3j'};
f2={'qf1e','qd2e','qf2a','qf2e','qd3e','qf4e','qf4b','qf4d','qd5d','qf6d','qf8d',...
    'sd1e','sf2e','sd1d','of1d','dq1d'};

light_speed = 2.99792458e08;
rf_harm_nb = 992;

%
% Several initialisations
%

try
    TangoHost = ['tango://' getenv('TANGO_HOST')];
    simdev=tango.Device([TangoHost '/sys/ringsimulator/ebs']);
    if nargin < 1
        ringfile=simdev.get_property('RingFile');
        ringpath=ringfile{1};
    end
    ringvarname=simdev.get_property('MatlabRingName');
    varname=ringvarname{1};
    
    if ischar(ringpath)
        [~,ringname]=fileparts(ringpath);
        r=load(ringpath);
        try
            ring=r.(varname); % use specified vvariable name
        catch
            ring=r.(ringname); %if not existing, use lattice file name
        end
        
        errs = r.errtab; % table of errors stored with lattice files
   else
        ring=ringpath;
        errs = atcreateerrortable(ring); % empty error table
    end
    
    bpms=atgetcells(ring,'FamName','BPM_.*');
    ids=atgetcells(ring,'FamName','ID\d\d$|^IDMarker');
    cavs = atgetcells(ring,'FamName','CA\d\d','CAV\w*');
    pins = atgetcells(ring,'FamName','PINHOLE_.*$|^PINHOLE\d\d');
    bpms_pins = bpms | pins;
    bpm_select = bpms(bpms_pins);
    pin_select = pins(bpms_pins);
    
    spos = findspos(ring,1:length(ring));
    
    lg = simu_init_lengths(atreduce(ring,bpms|ids),[f1 f3]);
    cellmag = simu_init_indices(ring,ids,[f1,f2]);
    
    [rel,tel,trand]=bpm_matrices(ring(bpms));
    [det_rel,det_tel,det_trand]=bpm_matrices(ring);
    
    lgq = [lg.qf1a,lg.qd2a,lg.qd3a,lg.qf4a,lg.qd5b,lg.qf6b,lg.qf8b];
    lgs = [lg.sd1a,lg.sf2a,lg.sd1b,lg.of1b,lg.sh1a,lg.sh2b,lg.sh3e];
    
    freq = atgetfieldvalues(ring(cavs),'Frequency');
    RFVoltage = atgetfieldvalues(ring(cavs),'Voltage');

    ring_length = findspos(ring,length(ring) + 1);
    delta_l_num = light_speed * rf_harm_nb;
    delta_l = (delta_l_num / freq(1)) - ring_length;
    
    send_indices(cellmag,simdev);
    
    simdev.RingName=ring{atgetcells(ring,'Class','RingParam')}.FamName;
    simdev.BpmPosition=findspos(ring,bpms);
    simdev.MagnetLength=[lgq,lgs,lg.qf1j,lg.qf2j,lg.qd3j,lg.dq1b,lg.dq2c];
    simdev.RfFrequency=freq(1);
    simdev.RfVoltage=sum(RFVoltage);
    simdev.IdMarkers=int32(find(ids)');
    
    tbt_res = simdev.TbT_InCoord().read';
    
    mixed_ctr = 0;
    mem_emit_h = 0;
    mem_emit_v = 0;
 
    pause(0.2);
    ok = true;
    
catch ME
    simdev.ErrMessage = error_display(ME);
    ok = false;
end

try
    while ok
        %
        % get new values from Tango world
        %
        try
            new_str = simdev.GetUpdStrengths();
            new_fields = simdev.GetUpdFields();
            atts = simdev.get_attribute('RfFrequency','TbT_BufferSize','TbT_InCoord','MatMode','AtxEvery','RfVoltage');
            
            new_freq = atts(1);
            tbt_buf_size = double(atts(2).set);
            tbt_res = atts(3).read';
            mode = atts(4).read;
            atx_every = atts(5).read;
            new_RFVoltage = atts(6);
            
        catch ME
            if isempty(strfind(ME.message,'OFF state'))
                rethrow(ME);
            end
        end
        %
        % Apply new value(s) on simulated equipment(s)
        %
        if new_freq.set ~= freq(1)
            ring = atsetfieldvalues(ring,find(cavs),'Frequency',new_freq.set);
            freq = new_freq.set;
            delta_l = (delta_l_num / new_freq.set) - ring_length;
        end
        if new_RFVoltage.set ~= sum(RFVoltage)
            ring = atsetfieldvalues(ring,find(cavs),'Voltage',new_RFVoltage.set/sum(cavs));
            RFVoltage = new_RFVoltage.set;
        end
        if ~isempty(new_fields.svalue)
            [ring,field_strs] = apply_new_field(ring,cellmag,new_fields);
        end
        
        % set strengths from simulator and add gradient errors
        [ring,strs] = apply_new_strength(ring,cellmag,new_str,errs);
        % Compute data according to mode
        % mode value: 0 --> TbT, 1 --> atlinopt, 2 --> atx, 3 --> Mixed, 4 --> detailed_atx
        %
        modename = {'tbt','atlinopt','atx','mixed','detailed atx'};
        try
            switch mode
                case 0
                    raw_data = ebs_tbt();
                    
                case 1
                    raw_data = ebs_atlinopt();
                    
                case 2
                    raw_data = ebs_atx();
                    
                case 3
                    raw_data = ebs_mixed();
                    
                case 4
                    raw_data = ebs_detailed_atx();
            end
            
        catch err
            disp(err)
            
            simdev.ErrMessage=[modename{mode+1} ' failed'];
                    
            raw_data = nodata;
        end
        %
        % Do we exit loop ?
        %
        ok = simdev.isState(tango.DevState.On);
        %
        % Send new data to Tango if the device is still On
        % Warning, the device could have been switched to OFF just between previous test and now!!
        % Also send to Tango the simulator strings of the modified equipments
        % If saving ring in a file has been requested, do it now
        %
        if ok
            try
                if mode == 0
                    simdev.PushTbTRawData(raw_data);
                elseif mode == 4
                    simdev.PushDetailedRawData(raw_data);
                else
                    simdev.PushRawData(raw_data);
                end
                
                if isfield(strs,'name')
                    send_strs(strs,simdev);
                end              
                if ~isempty(new_fields.svalue)
                    send_strs(field_strs,simdev);
                end
                
                fi = simdev.MatSaveRing();
                if ~isempty(fi.read)
                    save(fi.read,'ring');
                    simdev.MatSaveRing = 'done';
                end
            catch ME
                if isempty(strfind(ME.message,'OFF state'))
                    rethrow(ME);
                end
            end
        end
    end %main loop
catch ME
    simdev.ErrMessage = error_display(ME);
end


    function data = ebs_tbt()
            
        refpts=[bpms;true];  % Add length(ring)+1 to the reference points
           
        tbt_res_hv = NaN(sum(bpms),tbt_buf_size,2);    % Allocate space for the results

        for turn=1:tbt_buf_size
            tbt_res = linepass(ring,tbt_res(:,end),refpts);
            bpmnoise=simdev.BpmNoise.read;
            trand=num2cell(bpmnoise*ones(2,size(trand,2)),1);
            tbt_res([1 3],1:sum(bpms)) = bpm_process(tbt_res([1 3],1:sum(bpms)),rel,tel,trand);
            tbt_res_hv(:,turn,:)=tbt_res([1 3],1:end-1)';
        end
            
        data = [tbt_buf_size;tbt_res_hv(:)]';
       
    end


    function data = ebs_atlinopt()
        %
        % Compute one closed orbit point and delta_p first (given the delta_l due to frequency change)
        % Then compute linear optics providing the delta_p and closed orbit point just computed
        %
        [~,fixed_point] = findsyncorbit(ring,delta_l);
        [params,t,c] = atlinopt(ring,fixed_point(5),bpms_pins,fixed_point);
        
        params_pins = params(pin_select);
        params_bpms = params(bpm_select);
        
        clorb = cat(2,params_bpms.ClosedOrbit);
        bpmnoise=simdev.BpmNoise.read;
        trand=num2cell(bpmnoise*ones(2,size(clorb,2)),1);
        cororb = bpm_process(clorb([1 3],:),rel,tel,trand);
        clorb_pins = cat(2,params_pins.ClosedOrbit);
        
        beta = cat(1,params_bpms.beta);
        etah = cat(2,params_bpms.Dispersion);
        
        data = [t(1) t(2) c(1) c(2) 0.0 0.0 cororb(1,:) cororb(2,:) beta(:,1)' beta(:,2)' etah(1,:) etah(3,:) clorb_pins(1,:) clorb_pins(3,:)];
    end


    function data = ebs_atx()
        %
        % Compute one closed orbit point and delta_p first (given the delta_l due to frequency change)
        % Then compute linear optics providing the delta_p and closed orbit point just computed
        % In atx mode, we want beam data at bpm positions but also at pinhole positions
        %
        [~,fixed_point] = findsyncorbit(ring,delta_l);
        [beam,param] = atx(ring,fixed_point(5),bpms_pins);
        
        pins_beam = beam(pin_select);
        bpms_beam = beam(bpm_select);
        
        if isnan(param.fractunes(1)) == 0
            [sigs_x,sigs_z,sigs_xz] = arrayfun(@sel,pins_beam,'Uniform',1);
        else
            sigs_x = zeros(1,5);
            sigs_z = zeros(1,5);
            sigs_xz = zeros(1,5);
        end
        
        clorb = cat(2,bpms_beam.ClosedOrbit);
        bpmnoise=simdev.BpmNoise.read;
        trand=num2cell(bpmnoise*ones(2,size(clorb,2)),1);
        cororb=bpm_process(clorb([1 3],:),rel,tel,trand);
        clorb_pins = cat(2,pins_beam.ClosedOrbit);
        
        beta = cat(1,bpms_beam.beta);
        etah = cat(2,bpms_beam.Dispersion);
        
        data = [param.fractunes(1) param.fractunes(2)...
            param.chromaticity(1) param.chromaticity(2)...
            param.modemittance(1) param.modemittance(2)...
            cororb(1,:) cororb(2,:)...
            beta(:,1)' beta(:,2)'...
            etah(1,:) etah(3,:)...
            clorb_pins(1,:) clorb_pins(3,:)...
            sigs_x sigs_z sigs_xz];
    end


    function data = ebs_mixed()
        if mod(mixed_ctr,atx_every) == 0
            data = ebs_atx();
            mem_emit_h = data(5);
            mem_emit_v = data(6);
        else
            data = ebs_atlinopt();
            data(5) = mem_emit_h;
            data(6) = mem_emit_v;
        end
        
        mixed_ctr = mixed_ctr + 1;
    end


    function data = nodata()
        nbpm=size(bpms');
        npin=size(pins');
        data = [NaN NaN...
            NaN NaN...
            NaN NaN...
            NaN(nbpm) NaN(nbpm)...
            NaN(nbpm) NaN(nbpm)...
            NaN(nbpm) NaN(nbpm)...
            NaN(npin) NaN(npin)...
            NaN(nbpm) NaN(nbpm) NaN(nbpm)];
    end


    function data = ebs_detailed_atx()
        %
        % Compute one closed orbit point and delta_p first (given the delta_l due to frequency change)
        % Then compute linear optics providing the delta_p just computed
        %
        [~,fixed_point] = findsyncorbit(ring,delta_l);
        beam = atx(ring,fixed_point(5));
        
        clorb = cat(2,beam.ClosedOrbit);
        bpmnoise=simdev.BpmNoise.read;
        det_trand=num2cell(bpmnoise*ones(2,size(clorb,2)),1);
        cororb=bpm_process(clorb([1 3],:),det_rel,det_tel,det_trand);
        
        beta = cat(1,beam.beta);
        
        data = [size(spos,2) spos cororb(1,:) cororb(2,:) beta(:,1)' beta(:,2)'];
    end

    function [x,z,xz] = sel(data)
        x = data.beam66(1,1);
        z = data.beam66(3,3);
        xz = data.beam66(3,1);
    end

    function message=error_display(err)
        trace=arrayfun(@(bt) sprintf('%s, file: %s, line %d',bt.name,bt.file,bt.line),...
            err.stack,'UniformOutput',false);
        message=sprintf('%s\n',err.message,trace{:});
    end

end
