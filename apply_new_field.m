function [ring,all_fams]=apply_new_field(ring,cellmag,val)
% APPLY_NEW_FIELD(RING.CELLMAG,VAL)
%
% Set ring cell field.
% The function inputs (val) comes from the Tango world. It's a DevVarDoubleStringArray 
% describing cell new fields for one powersupply.
%
% The strings are:
%   - Matlab family name
%   For each value sent
%   	- Field name
% The doubles are:
%   - PS index in Matlab family
%   For each value sent:
%       - Value index in field
%       - Value
%
%RING:		AT structure
%CELLMAG:	Structure with ring indices for each magnets in each magnet families
%VAL:		The new field value
%
mat=reshape(val.dvalue(2:end),2,[]);
famname=val.svalue{1};
cells=val.dvalue(1);
minus=strfind(famname,'-');
if isempty(minus)       % Quadrupole, sextupole
    ids=cellmag.(lower(famname)){cells};
else                    % H/V steerer, skew quadrupole
    ids=cellmag.(lower(famname(1:minus-1))){cells};
end
[fam_strs,nbs]=cellfun(@(x,y) apply2(ids,x,y),num2cell(mat,1),val.svalue(2:end),'UniformOutput',false);
fam=struct('name',{famname},'fam_ind',{cells},'at_strs',{fam_strs(end)},'nbs',{nbs(end)});
all_fams={fam};

    function [simu_strs,nbs]=apply2(ids,field_data,field_name)
        field_index=field_data(1);
        field_value=field_data(2);
        ring(ids)=atsetfieldvalues(ring(ids),field_name,{field_index},field_value);
        simu_strs=arrayfun(@(id) at2str(ring{id}),ids,'UniformOutput',false);
        nbs=length(ids);
    end
end
